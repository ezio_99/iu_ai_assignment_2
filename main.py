from __future__ import annotations

import argparse
import concurrent
import math
import multiprocessing
import os
import random
import sys
from concurrent.futures import ALL_COMPLETED
from concurrent.futures.process import ProcessPoolExecutor
from copy import deepcopy
from functools import partial
from pathlib import Path
from typing import List
from datetime import datetime, timedelta

import numpy as np
import matplotlib.pyplot as plt
from PIL import Image, ImageChops, ImageDraw, ImageStat

parser = argparse.ArgumentParser(description="Approximate provided image drawing figures "
                                             "using evolutionary algorithm.")

# defaults for parser
MAX_GENERATIONS = 10000
COLORS = "random"
COLOR_OFFSET = 64
MAX_FIFURE_SIZE = 10
FIGURE = "circle"
POPULATION_SIZE = 5
STARTING_IMAGE = None
SAVE_PERIOD = 1000
GRAPH_SAVE_PERIOD = 5000
MAX_WORKERS = multiprocessing.cpu_count()
MODE = "single"
FIGURES_PER_MUTATION = 1

parser.add_argument("image", type=str,
                    help="Image for approximation.")
parser.add_argument("--max-generations", default=MAX_GENERATIONS, type=int,
                    help=f"Maximum number of generations. Default {MAX_GENERATIONS}.")
parser.add_argument("--colors", default=COLORS, type=str, choices=["random", "original"],
                    help="Which colors will be used in approximation. Random - pure random colors. "
                         "Original - colors from original image with random offset. "
                         f"Default {COLORS}.")
parser.add_argument("--color-offset", default=COLOR_OFFSET, type=int,
                    help="Maximum offset what will be used for randomizing original colors. "
                         f"Default {COLOR_OFFSET}.")
parser.add_argument("--max-figure-size", default=MAX_FIFURE_SIZE, type=int,
                    help="Maximum size of drawing figure in pixels. "
                         f"Default {MAX_FIFURE_SIZE}.")
parser.add_argument("--figure", type=str, default=FIGURE, choices=["circle", "triangle", "square", "romb"],
                    help="Using which figure image will be approximated. "
                         f"Default {FIGURE}.")
parser.add_argument("--population-size", default=POPULATION_SIZE, type=int,
                    help=f"Population size. Default {POPULATION_SIZE}.")
parser.add_argument("--starting-image", default=STARTING_IMAGE, type=str,
                    help=f"From which image start to approximate target."
                         f"Default is empty canvas with black color.")
parser.add_argument("--save-period", default=SAVE_PERIOD, type=int,
                    help=f"Results saving period in generations. Default {SAVE_PERIOD}.")
parser.add_argument("--graph-save-period", default=GRAPH_SAVE_PERIOD, type=int,
                    help=f"Fitness function changing graph saving period in generations. "
                         f"Default {GRAPH_SAVE_PERIOD}.")
parser.add_argument("--max-workers", default=MAX_WORKERS, type=int,
                    help="Maximum number of workers in parallel execution mode. "
                         f"Default {MAX_WORKERS}.")
parser.add_argument("--mode", default=MODE, type=str, choices=["single", "parallel"],
                    help=f"Execution mode. Default {MODE}.")
parser.add_argument("--figures_per_mutation", default=FIGURES_PER_MUTATION, type=int,
                    help="How many figures will be drawn per mutation. "
                         f"Default {FIGURES_PER_MUTATION}.")


def parse_args(parser: argparse.ArgumentParser) -> dict:
    """
    Function for parsing command line arguments.
    If some errors, exits with status code -1.
    """
    args = parser.parse_args()

    if not Path(args.image).exists():
        exit("Image does not exist", -1)

    if not args.starting_image is None and not Path(args.starting_image).exists():
        exit("Starting image does not exist", -1)

    if args.colors == "random":
        args.is_random_colors = True
    else:
        args.is_random_colors = False

    if args.mode == "single":
        args.parallel = False
    else:
        args.parallel = True

    del args.mode
    del args.colors

    return vars(args)


class Individual:
    """Class, representing individual in population"""

    __slots__ = ("image", "fitness")

    def __init__(self, image):
        self.image = image
        self.fitness = None

    def calculate_fitness(self, image):
        """Fitness is calculated using Root Mean Square"""
        self.fitness = get_rms(self.image, image)

    def __repr__(self):
        return f"Individual(fitness={self.fitness})"


def get_random_rgba(low=0, up=255, **kwargs):
    """Returns random RGBA color in [low, up] range"""
    return random.randint(low, up), random.randint(low, up), random.randint(low, up), random.randint(low, up)


def exit(message: str = "", status: int = 0):
    """Prints message and closes program with given status code"""
    print(message)
    sys.exit(status)


def randomize_color_rgba(color: np.array, max_color_offset: int) -> tuple:
    """Function for randomizing RGB part of RGBA color"""
    x = 255 - max(color[:3])
    y = -min(color[:3])
    x = min(max_color_offset, x)
    y = max(-max_color_offset, y)
    m = random.randint(min(x, y), max(x, y))
    return color[0] + m, color[1] + m, color[2] + m, color[3]


def get_rms(i1, i2):
    """Function for calculating Root Mean Square from 2 images' difference"""
    diff = ImageChops.difference(i1, i2)
    summed = ImageStat.Stat(diff).rms
    rms = sum(summed)
    return rms


def format_timedelta(t: timedelta) -> str:
    """Returns formatted string from timedelta object"""
    return f"{int(t.total_seconds() / 3600)} hours, " \
           f"{int(t.seconds / 60) % 60} minutes, " \
           f"{t.seconds + t.microseconds / 1e6} seconds"


def get_randomized_original_color(original_np: np.array, max_color_offset: int, **kwargs) -> tuple:
    """Get color from original image, randomize it and return"""
    x = kwargs["x"]
    y = kwargs["y"]
    return randomize_color_rgba(original_np[y][x], max_color_offset)


def save_image(gen: int, image: Image, prefix: str, suffix: str):
    """Save image"""
    folder = f"images/results/{prefix}-{suffix}"
    if not os.path.exists(folder):
        os.makedirs(folder)

    image.save(f"{folder}/after-{gen}-gen.png", "PNG")


def save_graph(gen: int, x: List[int], y: List[float], prefix: str, suffix: str):
    """Save graph"""
    folder = f"images/results/{prefix}-{suffix}/graphs"
    if not os.path.exists(folder):
        os.makedirs(folder)

    plt.close()
    fig = plt.figure(figsize=(6, 3), dpi=150)
    plt.plot(x, y)
    plt.xlabel("Generation")
    plt.ylabel("Fitness")
    fig.tight_layout()
    fig.savefig(f"{folder}/graph-after-{gen}-gen.png", dpi=200)


class Figures:
    """Class containing static methods for drawing different figures on image"""

    @staticmethod
    def square(x, y, max_figure_size):
        """Draw square"""
        K = random.randint(1, max_figure_size)
        return (x, y), (x + K, y), (x + K, y - K), (x, y - K)

    sqrt_3 = math.sqrt(3)

    @staticmethod
    def triangle(x, y, max_figure_size):
        """Draw triangle"""
        K = random.randint(1, max_figure_size)
        return (x, y), (x + K, y + int(Figures.sqrt_3 * K)), (x - K, y + int(Figures.sqrt_3 * K))

    @staticmethod
    def romb(x, y, max_figure_size):
        """Draw romb"""
        K = random.randint(1, max_figure_size)
        return (x, y), (x + K, y - K), (x, y - 2 * K), (x - K, y - K)

    figure_names_to_function = {}

    @staticmethod
    def draw(draw, figure: str, max_figure_size: int, color: tuple, x: int, y: int):
        """Draw figure by name"""

        if figure == "circle":
            draw.ellipse((x - max_figure_size, y - max_figure_size, x + max_figure_size, y + max_figure_size),
                         fill=color)
        else:
            draw.polygon(Figures.figure_names_to_function[figure](x, y, max_figure_size), fill=color)


Figures.figure_names_to_function = {
    "square": Figures.square,
    "romb": Figures.romb,
    "triangle": Figures.triangle
}


class ImageApproximation:
    """Class representing evolution algorithm for approximating image"""

    def __init__(self, image: str, max_generations: int, is_random_colors: bool,
                 color_offset: int, max_figure_size: int, figure: str,
                 population_size: int, starting_image: str,
                 max_workers: int, parallel: bool,
                 figures_per_mutation: int,
                 save_period: int, graph_save_period: int,
                 *args, **kwargs):

        # reading target image
        self.image_path = Path(image)
        if not self.image_path.exists():
            exit("Image does not exist", -1)

        # reading starting image
        self.starting_image = starting_image
        if starting_image is not None:
            self.starting_image_path = Path(starting_image)
            if not self.starting_image_path.exists():
                exit("Starting image does not exist", -1)

        # saving image as array
        self.original = Image.open(image).convert("RGBA")
        self.original_np = np.array(self.original)

        # initializing image shape
        self.height, self.width = self.original_np.shape[:2]

        # initializing variables related to evolutionary algorithm
        self.max_generations = max_generations
        self.population_size = population_size
        self.population: List[Individual] = []
        self.figures_per_mutation = figures_per_mutation

        # parameters related to drawing
        self.color_offset = color_offset
        self.max_figure_size = max_figure_size

        if is_random_colors:
            self.get_color = get_random_rgba
        else:
            self.get_color = partial(get_randomized_original_color,
                                     original_np=self.original_np, max_color_offset=self.color_offset)
        self.is_random_colors = is_random_colors

        self.figure = figure

        # parameters related to image and graph saving
        self.save_period = save_period
        self.graph_save_period = graph_save_period

        # parameters related to concurrent execution
        self.max_workers = max_workers
        if parallel:
            self.mutate_population = self.mutate_population_concurrent
        else:
            self.mutate_population = self.mutate_population_signle_thread

    def generate_zero_population(self):
        """Generates zero population of empty images"""
        self.population = []

        if self.starting_image is None:
            img = Image.new('RGBA', (self.width, self.height))
        else:
            img = Image.open(self.starting_image_path).convert("RGBA")

        self.population.append(Individual(img))
        for _ in range(self.population_size - 1):
            self.population.append(deepcopy(self.population[-1]))

        for i in self.population:
            i.calculate_fitness(self.original)

    def select(self) -> List[Individual]:
        """Select best individuals in population according to minimal fitness"""
        return sorted(self.population, key=lambda i: i.fitness)[:self.population_size]

    def mutate(self, ind: Individual) -> Individual:
        """
        Mutate individual adding to it randomly placed figure.
        Figure's color depends on which color mode (random or original) is selected.
        """
        ind = deepcopy(ind)
        draw = ImageDraw.Draw(ind.image)
        for _ in range(self.figures_per_mutation):
            x = random.randint(-self.max_figure_size, self.width - 1)
            y = random.randint(-self.max_figure_size, self.height - 1)
            color = self.get_color(x=x, y=y)
            Figures.draw(draw, self.figure, self.max_figure_size, color, x, y)

        ind.calculate_fitness(self.original)
        return ind

    def mutate_population_signle_thread(self) -> List[Individual]:
        """Mutate population using single thread."""
        return [self.mutate(i) for i in self.population]

    def mutate_population_concurrent(self) -> List[Individual]:
        """Mutate population using multiple processes."""
        fs = []

        with ProcessPoolExecutor(max_workers=self.max_workers) as executor:
            for i in self.population:
                fs.append(executor.submit(self.mutate, i))

            concurrent.futures.wait(fs, timeout=None, return_when=ALL_COMPLETED)

        return [f.result() for f in fs]

    def approximate_image(self):
        """Approximate image."""
        start = datetime.now()
        suffix = str(datetime.now()).split(".")[0].replace(" ", "-").replace(":", "-")
        prefix = f"{self.image_path.name}-{'random_colors' if self.is_random_colors else 'original_colors'}"

        self.generate_zero_population()

        x = []
        y = []

        start_gens = datetime.now()

        offspring = self.mutate_population()
        self.population.extend(offspring)

        for gen in range(1, self.max_generations + 1):
            self.population = self.select()

            if gen % self.save_period == 0:
                best = self.population[0]
                save_image(gen, best.image, prefix, suffix)

            if gen % self.graph_save_period == 0:
                save_graph(gen, x, y, prefix, suffix)

            print(f"Generation {gen} done. Best fitness = {self.population[0].fitness}")
            x.append(gen)
            y.append(self.population[0].fitness)

            offspring = self.mutate_population()
            self.population.extend(offspring)

            gen += 1

        end_gens = datetime.now()

        best = min(self.population, key=lambda ind: ind.fitness)
        result_filename = f"{self.image_path.parent}/{self.image_path.name[:self.image_path.name.rfind('.')]}_processed_" \
                          f"{'random_colors' if self.is_random_colors else 'original_colors'}.png"
        best.image.save(str(result_filename), "PNG")

        end = datetime.now()

        print(f"\nSaving result to {result_filename}")
        print(f"Total time: {format_timedelta(end - start)}")
        print(f"Time for {self.max_generations} generations: {format_timedelta(end_gens - start_gens)}")
        print(f"Avg time for generation: {format_timedelta((end_gens - start_gens) / self.max_generations)}")


def main():
    args = parse_args(parser)
    image_approximation = ImageApproximation(**args)
    image_approximation.approximate_image()


if __name__ == "__main__":
    main()
