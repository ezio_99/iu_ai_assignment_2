# Info about project

This repository contains solution for Assignment 2 for the course Introduction to AI (Spring 2021).

Approximate provided image drawing figures using evolutionary algorithm.

# Install dependencies

Required Python version is 3.8!

    pip install -r requirements 

# Usage

    python3 main.py
        [-h]
        [--max-generations MAX_GENERATIONS]
        [--colors {random,original}] 
        [--color-offset COLOR_OFFSET]
        [--max-figure-size MAX_FIGURE_SIZE]
        [--figure {circle,triangle,square,romb}] 
        [--population-size POPULATION_SIZE] 
        [--starting-image STARTING_IMAGE] 
        [--save-period SAVE_PERIOD]
        [--graph-save-period GRAPH_SAVE_PERIOD] 
        [--max-workers MAX_WORKERS] 
        [--mode {single,parallel}] 
        [--figures_per_mutation FIGURES_PER_MUTATION]
        image
# Positional arguments

    image               Image for approximation.

# Optional arguments

    -h, --help          show this help message and exit
  
    --max-generations MAX_GENERATIONS
                        Maximum number of generations. Default 10000.
    
    --colors {random,original}
                        Which colors will be used in approximation.
                        Random - pure random colors. 
                        Original - colors from original image with random offset. 
                        Default random.
  
    --color-offset COLOR_OFFSET
                        Maximum offset what will be used for randomizing original colors.
                        Default 64.
  
    --max-figure-size MAX_FIGURE_SIZE
                        Maximum size of drawing figure in pixels. Default 10.
  
    --figure {circle,triangle,square,romb}
                        Using which figure image will be approximated. Default circle.
  
    --population-size POPULATION_SIZE
                        Population size. Default 5.
  
    --starting-image STARTING_IMAGE
                        From which image start to approximate target.
                        Default is empty canvas with black color.
  
    --save-period SAVE_PERIOD
                        Results saving period in generations. Default 1000.
  
    --graph-save-period GRAPH_SAVE_PERIOD
                        Fitness function changing graph saving period in generations.
                        Default 5000.
  
    --max-workers MAX_WORKERS
                        Maximum number of workers in parallel execution mode. Default 8.
  
    --mode {single,parallel}
                        Execution mode. Default single.
  
    --figures_per_mutation FIGURES_PER_MUTATION
                        How many figures will be drawn per mutation. Default 1.
